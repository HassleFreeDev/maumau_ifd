
import java.util.*;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
//        Instantiate new players Obj
        Players p = new Players();
//        Create Player
        p.createPlayer();

//        Instantiate new cards Obj
        Cards c = new Cards();

//        Instantiate new playerDo Obj
        PlayerDo firstGame = new PlayerDo();

//        TODO: Looking for a way to assign 'names' to a unique variable
        String firstPlayer = (String) p.getPlayerName().get(0);
        String secondPlayer = (String) p.getPlayerName().get(1);
        String thirdPlayer = (String) p.getPlayerName().get(2);

//        TODO: Looking for a way to create "n" player with a loop
        Player player_1 = new Player(firstPlayer);
        Player player_2 = new Player(secondPlayer);
        Player player_3 = new Player(thirdPlayer);
        
        int i = 0;
        while (i < 5) {
//            TODO: loop trough "n" player via loop and assign a card to the handSheet
            firstGame.drawStartingHand(c, player_1);
            firstGame.drawStartingHand(c, player_2);
            firstGame.drawStartingHand(c, player_3);
            i++;
        }
        

//        TESTING PURPOSE
        System.out.println(player_1.getName() + "'s Hand: " + player_1.showOwnHandsheet());
        System.out.println(player_2.getName() + "'s Hand: " + player_2.showOwnHandsheet());
        System.out.println(player_3.getName() + "'s Hand: " + player_3.showOwnHandsheet());

        System.out.println(c);
        c.cardOnTable(c);

    }

    public static void startGame() {

    }

    public static void endGame() {
//        End the game
    }

    public static void restartGame() {
//        Reload everything
    }

    private static void drawStartingHand() {

    }

}
