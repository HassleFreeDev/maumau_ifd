// Holds ArrayList of the players

import java.util.ArrayList;
import java.util.Scanner;

public class Players {

    Scanner scanner = new Scanner(System.in);

    private ArrayList<String> player = new ArrayList<>();
    private int playerCount = 3;

    public ArrayList<String> createPlayer() {
        System.out.println("Wie lautet dein Name? (Leer lassen für NPC)!");
        System.out.println("1. Spieler muss ein Mensch sein");

//         Amount of players should be at least 3
        if (playerCount < 3) {
            playerCount = 3;
        }

        String playerName;
        int i = 1;
        while (i <= playerCount) {
//             if last iteration set npc automatically
//             if list contains NPC_1 set this to NPC_2
            if (i == playerCount) {
                if (player.contains("NPC_1")) {
                    playerName = "NPC_2";
                    player.add(playerName);
                    break;
                }

//                 if list doesn't contain NPC_1, set this to NPC_1
                playerName = "NPC_1";
                player.add(playerName);
                break;
            }

//            userInput for player name
            System.out.print("Name: ");
            playerName = scanner.nextLine();

//             depends on playerCount ~> how many players need to be human
            if (playerName.isEmpty() && i < (playerCount - 1)) {
                System.out.println(i + ". Spieler muss ein Mensch sein!");
                continue;
            }

//             let player decide if the next to last player is human or npc
            if (playerName.isEmpty() && i == (playerCount - 1)) {
                playerName = "NPC_1";
            }

            player.add(playerName);
            i++;
        }

        return player;
    }

    public ArrayList<String> getPlayerName() {
        return player;
    }
    
    public int getPlayerCount() {
        return this.playerCount;
    }
}
