// How can a player react, to what can he react

public class PlayerDo {

//    Can probably be omitted
    public void drawStartingHand(Cards cards, Player player) {
        String drawnCard = (String) cards.getCards().get(0);
        player.handSheet.add(drawnCard);
        cards.decreaseCards();
    }

    public void drawACard() {
//        'Draw' one card -> add a card to the this.handsheet and remove this card from the carddeck
    }

    public void drawNCards() {
//        this.drawACard -> multiple times. 
//        Depends on how many sevens are on the table e.g.
//        1x seven on the table = this.drawACard * 2;
//        2x seven on the table = this.drawACard * 4; and so on... (up to 4 times possible)
    }

    public void suspendRound() {
//        endTurn(); without doing anything else
//        will probably be called on the next player when an eight is played
    }

    public void whichCard() {
//        When jack is played
//        Change actual card on table to any card that you want
    }

    public void playCard() {
//        Take existing card from hand and add on top of the table
//        Iterate with for loop trough ArrayList.size() and check which card from the hand is playable with the card on the table
//        Set a number to every playable card and let the player choose via console by entering an possible (shown) integer
//        If no card is playable (loop returns 0) -> draw a card
//        Iterate trough hand and check if a card is now playable
//        If not -> endTurn();
    }
}
