import java.util.ArrayList;
import java.util.Collections;

public class Cards {
    
    private final ArrayList<String> cards = new ArrayList<>();
    private final String[] colorCards = {
        "Herz",
        "Karo",
        "Pik",
        "Kreuz"
    };
    private final String[] valueCards = {
        "Sieben",
        "Acht",
        "Neun",
        "Zehn",
        "Bube",
        "Dame",
        "König",
        "Ass"
    };

    public Cards() {
//        Create carddeck from the two arrays colorCards and valueCards
        createCards();
//        Shuffle created carddeck
        shuffleCards();
    }
    
    private ArrayList createCards() {
        int i = 0;
//        iterate trough every "color"
        for(String color : colorCards) {
//            iterate for every "color" trough every value
            for(String value : valueCards) {
//                create for every iteration a "card"
                cards.add(color + " " + value);
                i++;
            }
        }
        return cards;
    }
    
//    Get cards
    public ArrayList getCards() {
        return cards;
    }
    
//    Shuffle deck
    private void shuffleCards() {
        Collections.shuffle(cards);
    }
    
//    Decrease deck by one (card) if method is called
    public ArrayList decreaseCards() {
//        System.out.println("Entferne folgende Karte: " + cards.remove(0));
        cards.remove(0);
        
        return cards;
    }
    
    public void cardOnTable(Cards cards) {
        System.out.println("Aktuelle, offene Karte auf Tisch: " + cards.getCards().get(0));
    }
    
    @Override
    public String toString() {
//        Show acutal deck size when the object is called (e.g. System.out.println(card); )
        return "" + cards.size();
    }
}
