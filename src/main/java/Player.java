// the single player 
import java.util.ArrayList;
import java.util.Random;

public class Player {
    Players player = new Players();
    
    public String playerName;   
    public ArrayList<String> handSheet = new ArrayList<>();
    public int handSheetCount;
    public boolean human;
    public int winCount;
    public int lossLeftOverCards;

//     Constructor
    public Player(String playerName) {
        this.playerName = playerName;
        this.handSheet = new ArrayList<>();
        this.handSheetCount = 0;
        
        if("NPC_1".equals(playerName) || "NPC_2".equals(playerName)) {
            this.human = false;
        }
        
        this.winCount = 0;
        this.lossLeftOverCards = 0;
    }
    
    public String getName() {
        return playerName;
    }
//    can probably be omitted
    public void showName() {
        System.out.println(playerName);
    }
    
    public int getHandsheetCount() {
        return handSheetCount;
    }
//        can probably be omitted
    public void showHandSheetCount() {
        System.out.println(handSheetCount);
    }
    
//    To display the handSheetCount of the other Players call showHandSheetCount on their Obj
    
    public ArrayList<String> showOwnHandsheet() {
        return handSheet;
    }
    
//    isHuman don't need to be a method, because the cases are catched with an if statement in the constructor
    
    public int additionLeftOverValues() {
//      TODO:  add the hidden values from every card together
        return 1;
    }
    
//    num between 1 and playerCount to identify a random player
    public int generateRandomNum() {
        Random random = new Random();
        int min = 1;
        int max = player.getPlayerCount() + 1; // playerCount + 1 so every player has a chance to start the game
        int randomNum = random.nextInt(max - min) + min;
        return randomNum;
    }
     
    
//    Show playername when object is printed (e.g. System.out.println(player); )
    @Override
    public String toString() {
        return "" + playerName;
    }

}
