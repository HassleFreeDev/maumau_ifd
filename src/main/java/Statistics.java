// Simple statistics about wins, and loss value counts 
public class Statistics {
    private String namePlayer;
    private int wins;
    private int leftOverValuesSum;
    
    public Statistics() {
        this.namePlayer = "";
        this.wins = 0;
        this.leftOverValuesSum = 0;
    }
    
    public String getStatistic() {
        return "Spieler: " + namePlayer + ",\n" + "Wins: " + wins + ",\n" + "Left over cards value: " + leftOverValuesSum;
    }
    
//    This values should stored in a session, at least
}
